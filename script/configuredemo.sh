#!/bin/sh -

UART="/dev/ttyUSB0"
BAUDRATE=9600

PAUSA=10

echo
echo

echo "Configurando UART" $UART "a" $BAUDRATE "bps"
stty -F $UART $BAUDRATE

echo
echo

while [ 1 ]; do

   FULLTIME=`date +"%H:%M:%S"`
   HOUR=`echo $FULLTIME | cut -f1 -d:`
   MIN=`echo $FULLTIME | cut -f2 -d:`
   SEC=`echo $FULLTIME | cut -f3 -d:`
   
   TOW=`expr $HOUR \* 3600 + $MIN \* 60 + $SEC`

   echo " * Proceso de configuración en curso..."
   echo "      Hora actual     : " $FULLTIME
   echo "      TOW en segundos : " $TOW
   echo $TOW > /dev/ttyUSB0
   echo " * Hora configurada en el dispositivo, durmiendo durante" $PAUSA "segundos."
   echo

   sleep $PAUSA;

done

