/* Copyright 2016, Gerardo Puga
 *
 * This file is part of CIAA Firmware.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/** \brief ciaa_leon3_demo source file
 **
 **
 **/

/** \addtogroup CIAA_Firmware CIAA Firmware
 ** @{ */
/** \addtogroup Examples CIAA Firmware Examples
 ** @{ */
/** \addtogroup ciaa_leon3_demo ciaa_leon3_demo example source file
 ** @{ */

/*
 * Initials     Name
 * ---------------------------
 * GLP          Gerardo Puga
 * */

/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 * 20161203 v0.0.1   PR   first functional version
 */

/*==================[inclusions]=============================================*/


#include "ledsrow.h"
#include "grlib.h"


/*==================[macros and definitions]=================================*/


#define FULL_BAR_BITS 26

#define FULL_BAR_MASK ((1 << FULL_BAR_BITS) - 1)


/*==================[internal data declaration]==============================*/


/*==================[internal functions declaration]=========================*/


/*==================[internal data definition]===============================*/


grPlugAndPlayAPBDeviceTableEntryType apbDeviceLedsRow;

grDeviceAddress apbDeviceLedsRowAddr;

int32_t lonelyLedCounter = 0;

uint32_t redLedBar;

uint32_t greenLedBar;

uint32_t lonelyRedLed;

uint32_t barChangeCounter = 0;

uint32_t samplesSinceChange = 0;


uint32_t fullLedBar = 0;


enum { firstMovement, secondMovement, thirdMovement, fourthMovement, fifthMovement, movementsCount };


/*==================[external data definition]===============================*/


/*==================[internal functions definition]==========================*/


void ledsUpdateRows()
{

    lonelyLedCounter = (lonelyLedCounter + 1) % 10;

    lonelyRedLed = 0;

    if (lonelyLedCounter < 5)
    {
        lonelyRedLed = 1;
    }

    samplesSinceChange++;

    switch (barChangeCounter % movementsCount) {

        case firstMovement :

            fullLedBar = (fullLedBar >> 1) | (1 << (FULL_BAR_BITS - 1));

            if (samplesSinceChange >= 26)
            {
                barChangeCounter++;
                samplesSinceChange = 0;
            }

            break;

        case secondMovement :

            fullLedBar = (~fullLedBar) & FULL_BAR_MASK;

            if (samplesSinceChange >= 16)
            {
                barChangeCounter++;
                samplesSinceChange = 0;
            }

            break;

        case thirdMovement :

            fullLedBar = (FULL_BAR_MASK >> (2 * samplesSinceChange)) << samplesSinceChange;

            if (samplesSinceChange >= 11)
            {
                barChangeCounter++;
                samplesSinceChange = 0;
            }

            break;

        case fourthMovement :

            fullLedBar = ((fullLedBar >> 1) | ((fullLedBar & 0x01) << (FULL_BAR_BITS - 1))) & FULL_BAR_MASK;

            if (samplesSinceChange >= 26)
            {
                barChangeCounter++;
                samplesSinceChange = 0;
            }

            break;


        case fifthMovement :

            fullLedBar = ((fullLedBar << 1) | ((fullLedBar & (0x01 << (FULL_BAR_BITS - 1))) >> (FULL_BAR_BITS - 1))) & FULL_BAR_MASK;

            if (samplesSinceChange >= 12)
            {
                barChangeCounter++;
                samplesSinceChange = 0;

                fullLedBar = 0x00;
            }

            break;


    }

    redLedBar   = (fullLedBar >> 8);
    greenLedBar = fullLedBar & 0xff;

}

/*==================[external functions definition]==========================*/


void ledsRowInit()
{
    grWalkPlugAndPlayAPBDeviceTable(
            GRLIB_PNP_VENDOR_ID_GAISLER_RESEARCH,
            GRLIB_PNP_DEVICE_ID_GRGPIO,
            &apbDeviceLedsRow,
            3);

    apbDeviceLedsRowAddr = apbDeviceLedsRow.address;
}


void ledsRowTick()
{
    uint32_t ledsRowState;

    ledsRowState = (redLedBar << 9) | (lonelyRedLed << 8) | (greenLedBar);

    ledsUpdateRows();

    grRegisterWrite(
            apbDeviceLedsRowAddr,
            GRLIB_GRGPIO_OUTPUT_REGISTER,
            ledsRowState);
}


/* **************************** */

/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/

