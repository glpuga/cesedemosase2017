/* Copyright 2016, Gerardo Puga
 *
 * This file is part of CIAA Firmware.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/** \brief ciaa_leon3_demo source file
 **
 **
 **/

/** \addtogroup CIAA_Firmware CIAA Firmware
 ** @{ */
/** \addtogroup Examples CIAA Firmware Examples
 ** @{ */
/** \addtogroup ciaa_leon3_demo ciaa_leon3_demo example source file
 ** @{ */

/*
 * Initials     Name
 * ---------------------------
 * GLP          Gerardo Puga
 * */

/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 * 20161203 v0.0.1   PR   first functional version
 */

/*==================[inclusions]=============================================*/


#include "lcdhigh.h"
#include "lcdlow.h"
#include <stdio.h>
#include <stdarg.h>


/*==================[macros and definitions]=================================*/


#define CLEAR_DISPLAY           (0x01)
#define RETURN_HOME             (0x02)
#define DISPLAY_ONOFF(D,C,B)    (0x04 | (((uint32_t)(((D)<<2)|((C)<<1)|((B)<<0))) & 0x1c))
#define FUNCTION_SET(DL, N, F)  (0x20 | (((uint32_t)(((DL)<<4)|((N)<<3)|((F)<<2))) & 0x1c))
#define SET_CGRAM_ADDR(x)       (0x40 | (((uint32_t)(x))&0x3f))
#define SET_DDRAM_ADDR(x)       (0x80 | (((uint32_t)(x))&0x7f))


#define DISPLAY_PRINT_BUFFER_LEN    (DISPLAY_WIDTH + 1)


/*==================[internal data declaration]==============================*/


/*==================[internal functions declaration]=========================*/


/*==================[internal data definition]===============================*/


char lcdHighShadowBuffer[2][DISPLAY_WIDTH];

char lcdHighHotBuffer[2][DISPLAY_WIDTH];

char lcdHighTempBuffer[DISPLAY_PRINT_BUFFER_LEN];


/*==================[external data definition]===============================*/


/*==================[internal functions definition]==========================*/


/*==================[external functions definition]==========================*/


void lcdHighPrintLine(uint32_t row, const char *format, ...)
{
    va_list     vargs;
    uint32_t    len;

    /*
     * Inicializo la lista de argumentos de long variable */
    va_start(vargs, format);

    /*
     * Creo el texto con formato, me guardo al longitud resultante.
     * */
    len = vsnprintf(lcdHighTempBuffer, DISPLAY_PRINT_BUFFER_LEN, format, vargs);

    /*
     * vsnprintf NO devuelve la longitud de la cadena. Si el texto no cabe en el buffer de destino,
     * devuelve la longitud que HUBIERA TENIDO si HUBIERA ENTRADO.
     * */

    if (len >= DISPLAY_PRINT_BUFFER_LEN)
    {
        /*
         * El texto no cupo en el buffer.
         * */

        len = DISPLAY_PRINT_BUFFER_LEN - 1;
    }

    /*
     * Si quedó espacio libre, completo con espacios en blanco para sobreescribir
     * lo que hubiera en pantalla de antes.
     * */

    for (; len < (DISPLAY_PRINT_BUFFER_LEN - 1); len++)
    {
        lcdHighTempBuffer[len] = 0x20;
    }

    /*
     * Finalizo la cadena en cero, por si las dudas.
     * */

    lcdHighTempBuffer[len] = 0;

    va_end(vargs);

    /*
     * Actualizo el hotbuffer.
     * */

    strncpy(lcdHighHotBuffer[row], lcdHighTempBuffer, DISPLAY_WIDTH);
}


void lcdHighClear()
{
    int i;

    // WAIT_FOR_BF_CLEAN();

    /*
     * Limpio el contenido de la memoria del
     * lcdHigh por comando.
     * */

    lcdLowWriteInstructionByte(CLEAR_DISPLAY);

    /*
     * Sincronizo el estado de los buffers internos para que coincidan con
     * el lcdHigh.
     * */

    for (i = 0; i < DISPLAY_WIDTH; i++)
    {
        lcdHighHotBuffer[0][i]      = ' ';
        lcdHighHotBuffer[1][i]      = ' ';

        lcdHighShadowBuffer[0][i]   = ' ';
        lcdHighShadowBuffer[1][i]   = ' ';
    }
}


void lcdHighUpdate()
{
    uint32_t i, o;
    int32_t changeStart, changeEnd;

    for (o = 0; o < DISPLAY_HEIGHT; o++)
    {
        /*
         * Para minimizar el tiempo que toma la actualización sólo escribo en el lcd lo que está
         * entre la primera modificación y la última del cada renglón respecto de lo que está en
         * la memoria del lcd.
         *
         * Para saber qué es lo que hay en memoria del lcd, tengo una copia local almacenada en
         * lcdHighShadowBuffer.
         */

        changeStart = -1;
        changeEnd   = -1;

        /*
         * Busco el primer caracter que habría que actualizar del renglón.
         * */

        for (i = 0; i < DISPLAY_WIDTH; i++)
        {
            if (lcdHighHotBuffer[o][i] != lcdHighShadowBuffer[o][i])
            {
                changeStart = i;
                changeEnd   = i;
                break;
            }
        }

        /*
         * Busco el último caracter que habría que actualizar del renglón.
         * */

        for (; i < DISPLAY_WIDTH; i++)
        {
            if (lcdHighHotBuffer[o][i] != lcdHighShadowBuffer[o][i])
            {
                changeEnd = i;
            }
        }

        /*
         * Actualizo todo lo que está entre el primer y el último caracter que habría que actualizar.
         * Si changeStart = -1, significa que no había diferencias en este renglón.
         * */
        if (changeStart >= 0)
        {
            switch(o)
            {
                case 0:
                    /*
                     * Renglón 1, columna del primer cambio.
                     * */
                    lcdLowWriteInstructionByte(SET_DDRAM_ADDR(0x00 + changeStart));
                    break;

                default:
                    /*
                     * Renglón 1, columna del primer cambio.
                     * */
                    lcdLowWriteInstructionByte(SET_DDRAM_ADDR(0x40 + changeStart));
                    break;
            }

            for (i = changeStart; i <= changeEnd; i++)
            {
                // WAIT_FOR_BF_CLEAN();

                /*
                 * Actualizo el caracter en pantalla.
                 * */
                lcdLowWriteDataByte((uint8_t)lcdHighHotBuffer[o][i]);

                /*
                 * Sincronizo el hotBuffer con el shadowBuffer.
                 * */
                lcdHighShadowBuffer[o][i] = lcdHighHotBuffer[o][i];
            }
        }
    }
}


void lcdHighInit()
{
    lcdLowInit();
    lcdHighClear();
}


/* **************************** */

/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/


