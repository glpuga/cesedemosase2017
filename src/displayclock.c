/* Copyright 2016, Gerardo Puga
 *
 * This file is part of CIAA Firmware.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/** \brief ciaa_leon3_demo source file
 **
 **
 **/

/** \addtogroup CIAA_Firmware CIAA Firmware
 ** @{ */
/** \addtogroup Examples CIAA Firmware Examples
 ** @{ */
/** \addtogroup ciaa_leon3_demo ciaa_leon3_demo example source file
 ** @{ */

/*
 * Initials     Name
 * ---------------------------
 * GLP          Gerardo Puga
 * */

/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 * 20161203 v0.0.1   PR   first functional version
 */

/*==================[inclusions]=============================================*/


#include "displayclock.h"
#include "grlib.h"


/*==================[macros and definitions]=================================*/


/*==================[internal data declaration]==============================*/


/*==================[internal functions declaration]=========================*/


/*==================[internal data definition]===============================*/


grPlugAndPlayAPBDeviceTableEntryType apbDeviceDisplayA;

grPlugAndPlayAPBDeviceTableEntryType apbDeviceDisplayB;

grDeviceAddress apbDeviceDisplayAAddr;

grDeviceAddress apbDeviceDisplayBAddr;

int32_t timeOfWeekSeconds;

int32_t timeOfWeekIsValid;


/*==================[external data definition]===============================*/


/*==================[internal functions definition]==========================*/


uint32_t displayClockDisplayDigit(uint32_t digit)
{
    uint32_t retvalue;

    retvalue = 0;

    switch (digit) {

        case 0 :
            retvalue = 0x3f;
            break;

        case 1 :
            retvalue = 0x06;
            break;

        case 2 :
            retvalue = 0x5b;
            break;

        case 3 :
            retvalue = 0x4f;
            break;

        case 4 :
            retvalue = 0x66;
            break;

        case 5 :
            retvalue = 0x6d;
            break;

        case 6 :
            retvalue = 0x7d;
            break;

        case 7 :
            retvalue = 0x07;
            break;

        case 8 :
            retvalue = 0x7f;
            break;

        case 9 :
            retvalue = 0x6f;
            break;

        case 10 :
            retvalue = 0x76;
            break;

        case 11 :
            retvalue = 0x6d;
            break;

        default :
            retvalue = 0x6d;
            break;
    }

    return retvalue;
}


/*==================[external functions definition]==========================*/


void displayClockInit()
{
    grWalkPlugAndPlayAPBDeviceTable(
            GRLIB_PNP_VENDOR_ID_GAISLER_RESEARCH,
            GRLIB_PNP_DEVICE_ID_GRGPIO,
            &apbDeviceDisplayA,
            0);

    apbDeviceDisplayAAddr = apbDeviceDisplayA.address;

    grWalkPlugAndPlayAPBDeviceTable(
            GRLIB_PNP_VENDOR_ID_GAISLER_RESEARCH,
            GRLIB_PNP_DEVICE_ID_GRGPIO,
            &apbDeviceDisplayB,
            1);

    apbDeviceDisplayBAddr = apbDeviceDisplayB.address;

    timeOfWeekSeconds = 0;
    timeOfWeekIsValid = 0;
}


void displayClockTick()
{
    uint32_t hs, min, sec;
    uint32_t hsmin, sechs;
    uint32_t blinkMask;
    static uint32_t decimatorCounter = 0;

    decimatorCounter = decimatorCounter + 1;


    /*
     * Por defecto apagar todos los dígitos.
     * */

    blinkMask = 0x00000000;

    if ((timeOfWeekIsValid == 1) || ((decimatorCounter & 0x01) == 0))
    {
        /*
         * Mostrar los dígitos tal cual están.
         * */
        blinkMask = 0xffffffff;
    }


    if (decimatorCounter >= 10)
    {
        timeOfWeekSeconds++;
        decimatorCounter = 0;
    }

    hs = timeOfWeekSeconds / 3600;

    min = (timeOfWeekSeconds - hs * 3600) / 60;

    sec = timeOfWeekSeconds % 60;

    hsmin = 0;
    hsmin = (hsmin << 8) | displayClockDisplayDigit(hs / 10);
    hsmin = (hsmin << 8) | displayClockDisplayDigit(hs % 10);
    hsmin = (hsmin << 8) | displayClockDisplayDigit(min / 10);
    hsmin = (hsmin << 8) | displayClockDisplayDigit(min % 10);

    sechs = 0;
    sechs = (sechs << 8) | displayClockDisplayDigit(sec / 10);
    sechs = (sechs << 8) | displayClockDisplayDigit(sec % 10);
    sechs = (sechs << 8) | displayClockDisplayDigit(10);
    sechs = (sechs << 8) | displayClockDisplayDigit(11);

    /*
     * Agrego el parpadeo.
     */

    hsmin = hsmin & blinkMask;
    sechs = sechs & blinkMask;

    /*
     * Los leds están conectados como ánodo común.
     */

    hsmin = hsmin ^ 0x7f7f7f7f;
    sechs = sechs ^ 0x7f7f7f7f;

    grRegisterWrite(
            apbDeviceDisplayBAddr,
            GRLIB_GRGPIO_OUTPUT_REGISTER,
            hsmin);

    grRegisterWrite(
            apbDeviceDisplayAAddr,
            GRLIB_GRGPIO_OUTPUT_REGISTER,
            sechs);
}


void displayClockSetTimeOfWeek(int32_t newTimeOfWeek)
{
    timeOfWeekSeconds = newTimeOfWeek;
    timeOfWeekIsValid = 1;
}



/* **************************** */

/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/

