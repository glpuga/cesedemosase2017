/* Copyright 2016, Gerardo Puga
 *
 * This file is part of CIAA Firmware.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/** \brief ciaa_leon3_demo source file
 **
 **
 **/

/** \addtogroup CIAA_Firmware CIAA Firmware
 ** @{ */
/** \addtogroup Examples CIAA Firmware Examples
 ** @{ */
/** \addtogroup ciaa_leon3_demo ciaa_leon3_demo example source file
 ** @{ */

/*
 * Initials     Name
 * ---------------------------
 * GLP          Gerardo Puga
 * */

/*
 * modification history (new versions first)
 * -----------------------------------------------------------
 * 20161203 v0.0.1   PR   first functional version
 */

/*==================[inclusions]=============================================*/


#include "lcdhigh.h"


/*==================[macros and definitions]=================================*/


#define TITLE_TEXT      "_CIAA FW INSIDE_"

#define ROLLING_TEXT    "CIAA LEON3 en sistema basado en Terasic DE2-115 (Cyclone IV) + GRLIB <-> "


/*==================[internal data declaration]==============================*/


/*==================[internal functions declaration]=========================*/


/*==================[internal data definition]===============================*/


const char lcdVisualizationFullLengthRollingText[] = ROLLING_TEXT;

char lcdVisualizationRollingTextSlice[DISPLAY_WIDTH + 1];

int32_t lcdVisualizationRollingTextSliceStartIndex = 0;


/*==================[external data definition]===============================*/


/*==================[internal functions definition]==========================*/


/*==================[external functions definition]==========================*/


void lcdVisualizationInit()
{
    lcdHighInit();

    lcdHighClear();
}


void lcdVisualizationTick()
{
    static int32_t uptime = 0;
    int32_t i, o;

    /*
     * Copio el texto rotante del buffer original al buffer intermedio que uso para imprimir en el display.
     */

    o = lcdVisualizationRollingTextSliceStartIndex;

    for (i = 0; i < DISPLAY_WIDTH; i++)
    {
        lcdVisualizationRollingTextSlice[i] = lcdVisualizationFullLengthRollingText[o];

        o++;

        if (lcdVisualizationFullLengthRollingText[o] == 0)
        {
            o = 0;
        }
    }

    lcdVisualizationRollingTextSliceStartIndex++;

    if (lcdVisualizationFullLengthRollingText[lcdVisualizationRollingTextSliceStartIndex] == 0)
    {
        lcdVisualizationRollingTextSliceStartIndex = 0;
    }


    lcdHighPrintLine(0, "%s", TITLE_TEXT);

    lcdHighPrintLine(1, "%s", lcdVisualizationRollingTextSlice);


    lcdHighUpdate();

    uptime++;
}


/* **************************** */

/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/


