
#include <stdint.h>
#include "grlib.h"
#include "lcdlow.h"


#define BOARD_PIN_LCD_EN            8
#define BOARD_PIN_LCD_RS            9
#define BOARD_PIN_LCD_RW            10


#define BOARD_TIME_LCD_SETUP_TIME   5
#define BOARD_TIME_LCD_HOLD_TIME    5


grPlugAndPlayAPBDeviceTableEntryType apbDeviceLCD;

grDeviceAddress apbDeviceLCDAddr;

uint32_t lcdLowControlPortShadow;


void lcdLowMicroDelay(int32_t us)
{
    volatile int32_t busyWork;
    int32_t cnt;

    /*
     * Groseramente calculado para un oscilador de 50 MHz.
     * */

    for (cnt = 0; cnt < 5 * us; cnt++)
    {
        asm(" nop ");
        asm(" nop ");
        asm(" nop ");
        asm(" nop ");
        asm(" nop ");

        asm(" nop ");
        asm(" nop ");
        asm(" nop ");
        asm(" nop ");
        asm(" nop ");
    }
}


void lcdLowDelay(int32_t ms)
{
    int32_t i;

    for (i = 0; i < ms; i++)
    {
        lcdLowMicroDelay(1000);
    }
}


void lcdLowSetRWValue(uint32_t value)

{
    if (value)
    {
        lcdLowControlPortShadow = lcdLowControlPortShadow | (1 << BOARD_PIN_LCD_RW);
    } else {
        lcdLowControlPortShadow = lcdLowControlPortShadow & (~(1 << BOARD_PIN_LCD_RW));
    }

    grRegisterWrite(
            apbDeviceLCDAddr,
            GRLIB_GRGPIO_OUTPUT_REGISTER,
            lcdLowControlPortShadow);
}


void lcdLowSetRSValue(uint32_t value)
{
    if (value)
    {
        lcdLowControlPortShadow = lcdLowControlPortShadow | (1 << BOARD_PIN_LCD_RS);
    } else {
        lcdLowControlPortShadow = lcdLowControlPortShadow & (~(1 << BOARD_PIN_LCD_RS));
    }

    grRegisterWrite(
            apbDeviceLCDAddr,
            GRLIB_GRGPIO_OUTPUT_REGISTER,
            lcdLowControlPortShadow);
}


void lcdLowSetENValue(uint32_t value)
{

    if (value)
    {
        lcdLowControlPortShadow = lcdLowControlPortShadow | (1 << BOARD_PIN_LCD_EN);
    } else {
        lcdLowControlPortShadow = lcdLowControlPortShadow & (~(1 << BOARD_PIN_LCD_EN));
    }

    grRegisterWrite(
            apbDeviceLCDAddr,
            GRLIB_GRGPIO_OUTPUT_REGISTER,
            lcdLowControlPortShadow);
}


uint32_t lcdLowGetDataPortValue()
{
    return grRegisterRead(
            apbDeviceLCDAddr,
            GRLIB_GRGPIO_DATA_REGISTER);
}


void lcdLowSetDataPortValue(uint32_t value)
{
    lcdLowControlPortShadow = (lcdLowControlPortShadow & 0xffffff00) | (value & 0xff);

    grRegisterWrite(
            apbDeviceLCDAddr,
            GRLIB_GRGPIO_OUTPUT_REGISTER,
            lcdLowControlPortShadow);
}

void lcdLowSetDataPortAsInput()
{
    grRegisterWrite(
            apbDeviceLCDAddr,
            GRLIB_GRGPIO_DIRECTION_REGISTER,
            0xffffff00);
}


void lcdLowSetDataPortAsOutput()
{
    grRegisterWrite(
            apbDeviceLCDAddr,
            GRLIB_GRGPIO_DIRECTION_REGISTER,
            0xffffffff);
}


uint32_t lcdLowReadNibble(uint32_t rs)
{
    uint32_t nibble;

    /* ******************************************************************************** */
    /* lo hago siguiendo las indicaciones de temporización del fabricante, de la hoja de datos
     * del ST7066                     */
    /* ******************************************************************************** */

    /*
     * Me aseguro que el bus de datos esté como entrada.
     * */

    lcdLowSetDataPortAsInput();

    /*
     * En este punto asumo que EN está en bajo.
     * */

    lcdLowSetRWValue(1); /* lectura */
    lcdLowSetRSValue(rs); /* el valor de rs me lo pasan como argumento */

    /*
     * Espero un tiempo de setup.
     * */
    lcdLowMicroDelay(BOARD_TIME_LCD_SETUP_TIME);

    /*
     * Aplico un flanco ascendente del EN para que el LCD tome los valores de RS y RW.
     * */

    lcdLowSetENValue(1);

    /*
     * Espero un tiempo de hold para cumplir con Tddr y con Tpw.
     * */

    lcdLowMicroDelay(BOARD_TIME_LCD_HOLD_TIME);

    /*
     * Según la hoja de datos, ya puedo leer el nibble desde el bus de datos
     * */

    nibble = (lcdLowGetDataPortValue() >> 4) & 0x0f;

    /*
     * Desactivo la línea EN.
     * */

    lcdLowSetENValue(0);

    /*
     * Introduzco una pausa adicional para asegurarme de que el LCD haya
     * completado la operación internamente.
     * */

    lcdLowMicroDelay(BOARD_TIME_LCD_HOLD_TIME);

    /*
     * Al salir EN  está en bajo, y el bus de datos como entradas,
     * igual que como cuando entré.
     * */

    return nibble;
}


void lcdLowWriteNibble(uint32_t rs, uint32_t nibble)
{
    /* ******************************************************************************** */
    /* lo hago siguiendo las curvas de temporización del fabricante, de la hoja de datos
     * del ST7066                     */
    /* ******************************************************************************** */

    /*
     * Asumo que EN está en bajo.
     * */

    lcdLowSetRWValue(0);      /* Escritura. */
    lcdLowSetRSValue(rs);

    /*
     * Espero un tiempo de setup.
     * */

    lcdLowMicroDelay(BOARD_TIME_LCD_SETUP_TIME);

    /*
     * Aplico un flanco ascendente del EN para que tome los valores.
     * */

    lcdLowSetENValue(1);

    /*
     * Espera de tiempo de hold.
     * */

    lcdLowMicroDelay(BOARD_TIME_LCD_HOLD_TIME);

    /*
     * Pongo el nibble a escribir en el bus de datos.
     * */

    lcdLowSetDataPortAsOutput();

    lcdLowSetDataPortValue((nibble & 0xf) << 4);

    /*
     * Tiempo de setup de datos.
     * */

    lcdLowMicroDelay(BOARD_TIME_LCD_SETUP_TIME);

    /*
     * Flanco de bajada de EN para que el LCD tome los datos.
     * */

    lcdLowSetENValue(0);

    /*
     * Tiempo de hold de los datos.
     * */

    lcdLowMicroDelay(BOARD_TIME_LCD_HOLD_TIME);

    /*
     * Lo que sigue no está en el diagrama de temporización, pero es por seguridad.
     *
     * Pongo de vuelta las líneas del bus de datos como entradas y espero un tiempo de hold
     * para que eso tome efecto.
     *
     *
     * */

    lcdLowSetDataPortAsInput();

    lcdLowMicroDelay(BOARD_TIME_LCD_HOLD_TIME);

    /*
     * Al salir EN  está en bajo, igual que al comenzar.
     *
     * */
}


void lcdLowWriteDataByte(uint32_t byte)
{

    /*
     * Nibble alto.
     * */

    lcdLowWriteNibble(1, (byte >> 4) & 0x0f);

    /*
     * Nibble bajo.
     * */

    lcdLowWriteNibble(1, byte & 0x0f);
}


void lcdLowWriteInstructionByte(uint8_t byte)
{

    /*
     * Nibble alto.
     * */

    lcdLowWriteNibble(0, (byte >> 4) & 0x0f);

    /*
     * Nibble bajo.
     * */

    lcdLowWriteNibble(0, byte & 0x0f);
}


void lcdLowInit()
{

    /* Inicializo la interfaz con el LCD */
    grWalkPlugAndPlayAPBDeviceTable(
            GRLIB_PNP_VENDOR_ID_GAISLER_RESEARCH,
            GRLIB_PNP_DEVICE_ID_GRGPIO,
            &apbDeviceLCD,
            2);

    apbDeviceLCDAddr = apbDeviceLCD.address;

    lcdLowControlPortShadow = 0;

    lcdLowSetDataPortAsInput();

    /* ******************************************************* */
    /* sigo las recomendaciones de la hoja de datos del ST7066 */
    /* ******************************************************* */

    /* me aseguro de que EN esté inicialmente en nivel bajo. Recordar que
     * según los diagramas de temporización de la hoja de datos del ST7066
     * la línea de EN tiene efectos en los dos flancos, y su estado inicial
     * al realizar una operación es en nivel BAJO  */

    lcdLowSetENValue(0);

    /* Espero al menos 15 ms después del encendido */
    lcdLowDelay(20);

    /* escribo la primera palabra de configuración */
    lcdLowWriteNibble(0, 0x03);

    /* espero al menos 5ms antes del siguiente comando  */
    lcdLowDelay(10);

    /* escribo la segunda palabra de configuración */
    lcdLowWriteNibble(0, 0x03);

    /* espero  más de 100uS antes del siguiente comando  */
    lcdLowDelay(10);

    /* escribo la tercera palabra de configuración */
    lcdLowWriteNibble(0, 0x03);

    /* espero  más de 100uS antes del siguiente comando  */
    lcdLowDelay(10);

    /* escribo la tercera palabra de configuración */
    lcdLowWriteNibble(0, 0x02); /* <<-- Acá cambio el modo por el de 4 bits (*********) */

    /* a partir de ahora se pueden usar las rutinas regulares de acceso en modo de 4 bits,
     * y para esta inicialización no se requieren tiempos intermedios (la hoja de datos no
     * especifica ninguno) */

    lcdLowDelay(10);

    lcdLowWriteInstructionByte(0x2b); /* 4 bits, 2 líneas, font de 5x10 */

    lcdLowDelay(10);

    lcdLowWriteInstructionByte(0x0c); /* encender el display, no mostrar el cursor, no parpadear el cursor */

    lcdLowDelay(10);

    lcdLowWriteInstructionByte(0x01); /* limpiar el display */

    lcdLowDelay(10);

    lcdLowWriteInstructionByte(0x06); /* autoincremento, sin desplazamiento */

    lcdLowDelay(10);
}


