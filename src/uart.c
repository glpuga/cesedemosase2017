/* Copyright 2016, Gerardo Puga
 *
 * This file is part of CIAA Firmware.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/** \brief ciaa_leon3_demo source file
 **
 **
 **/

/** \addtogroup CIAA_Firmware CIAA Firmware
 ** @{ */
/** \addtogroup Examples CIAA Firmware Examples
 ** @{ */
/** \addtogroup ciaa_leon3_demo ciaa_leon3_demo example source file
 ** @{ */

/*
 * Initials     Name
 * ---------------------------
 * GLP          Gerardo Puga
 * */

#include "uart.h"
#include "os.h"
#include "ciaaPOSIX_stdio.h"
#include "ciaaPOSIX_string.h"
#include "ciaak.h"


void uartInit(uartHandlerType *uartHandler, const char *devName, int32_t baudrate)
{
    uartHandler->filedesc = ciaaPOSIX_open(devName, ciaaPOSIX_O_RDWR);
    uartHandler->baudrate = baudrate;

    ciaaPOSIX_ioctl(uartHandler->filedesc, ciaaPOSIX_IOCTL_SET_BAUDRATE, (void *)baudrate);
}

void uartSendStringBlocking(uartHandlerType *uartHandler, const char *string)
{
    int32_t retValue, stringLen;

    stringLen = ciaaPOSIX_strlen(string);

    retValue = ciaaPOSIX_write(uartHandler->filedesc, string, stringLen);

    if (retValue < stringLen)
    {
        ShutdownOS(1);
    }
}

void uartReadByteBlocking(uartHandlerType *uartHandler, uint8 *dataPtr)
{
    int32_t retValue;

    retValue = ciaaPOSIX_read(uartHandler->filedesc, (void *)dataPtr, 1);

    if (retValue <= 0)
    {
        ShutdownOS(1);
    }
}

void uartSendByteBlocking(uartHandlerType *uartHandler, uint8 data)
{
    int32_t retValue;

    retValue = ciaaPOSIX_write(uartHandler->filedesc, (void *)&data, 1);

    if (retValue <= 0)
    {
        ShutdownOS(1);
    }
}

void uartSendBlocking(uartHandlerType *uartHandler, uint8 *dataPtr, int32_t size)
{
    int32_t retValue;

    retValue = ciaaPOSIX_write(uartHandler->filedesc, (void *)dataPtr, size);

    if (retValue <= 0)
    {
        ShutdownOS(1);
    }
}
