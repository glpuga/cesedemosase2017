
Setting up the Terasic DE2-115 hardware
---------------------------------------

This example requires an APBVGA ip core to be present within the
LEON3 system. 

In order to build a system based on the DE2-115 that is able to run this example
replace the leon3mp.vhd and config.vhd files that are located on the
designs/leon3-terasic-de2-115 folder of the GRLIB source code with the
files given with this example, and then execute:

# make quartus-clean
# make quartus

This will take a while. After the place&route finishes the folder will contain
the *.pof and *.sof files that can be programmed on the DE2-115 to setup a 
LEON3 system with an APBVGA module on the DE2-115.


MKPROM2 Configuration
---------------------

The following configuration from MKPROM generates a ROM image for the DE2-115
that can be flashed on the system previously created. 

Before running the following command make sure that you previously compiled
the example with "make clean; make generate; make all". When the compilation
is complete, go to the "out/bin" folder of the CIAA Firmware source and 
execute mkprom as follows:

mkprom2 -leon3 -msoft-float -mcpu=v8 -freq 50 -romsize 8192 -romwidth 8 -romws 4 -nosram -cas 2 -col 10 -sdram 128 -rmw -rstaddr 0x00000000 -baud 115200 -ccprefix sparc-rtems -nocomp -v  "cesedemosase2017.exe" -o"flashimage.exe"
