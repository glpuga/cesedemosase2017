
#ifndef BOARDSUPPORT_H_
#define BOARDSUPPORT_H_

#include <stdint.h>

void lcdLowInit();

void lcdLowWriteInstructionByte(uint8_t byte);

void lcdLowWriteDataByte(uint32_t byte);

void lcdLowWriteDataByte(uint32_t byte);

void lcdLowWriteInstructionByte(uint8_t byte);

#endif /* BOARDSUPPORT_H_ */
